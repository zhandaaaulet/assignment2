package com.company;

import javax.swing.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class MyApplication {
    // users - a list of users
    private ArrayList<User> userList;
    private Scanner sc = new Scanner(System.in);
    private User signedUser;

    public MyApplication() throws FileNotFoundException {
        userList = new ArrayList<>();
        fillUsers();
    }

    private void fillUsers() throws FileNotFoundException {
        File file = new File("C:\\Users\\ACER\\Desktop\\db.txt");
        Scanner fsc = new Scanner(file);
        while(fsc.hasNext()) {
            userList.add(new User(fsc.nextInt(), fsc.next(), fsc.next(), fsc.next(), fsc.next()));
        }
    }

    private void addUser(User user) {
        userList.add(user);

    }


    private void menu() throws FileNotFoundException, UnsupportedEncodingException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            } else {
                userProfile();
            }
        }
    }

    private void userProfile() throws FileNotFoundException, UnsupportedEncodingException {
        while (true) {
            System.out.println("Hello " + signedUser.getName() + " " + signedUser.getSurname());
            System.out.println("1) Log off");
            int choice = sc.nextInt();
            if (choice == 1) {
                logOff();
                break;
            }
        }
    }

    private void logOff() throws FileNotFoundException, UnsupportedEncodingException {
        signedUser = null;
    }

    private void authentication() throws FileNotFoundException, UnsupportedEncodingException {
        // sign in
        // sign up
        System.out.println("1) Sign in: ");
        System.out.println("2) Sign up: ");
        System.out.println("3) Exit");
        int choice = sc.nextInt();
        if (choice == 1) {
            signIn();

        } else if (choice == 2) {
            signUp();
        }
    }


    private void signIn() throws FileNotFoundException, UnsupportedEncodingException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write your username: ");
        String username = sc.next();
        System.out.println("Write your password: ");

        Password password = new Password(sc.next());
        for (User user : userList) {
            if (username.equals(user.getUsername()) && password.equals(user.getPassword())) {
                signedUser = user;
                return;
            }
        }

        System.out.println("Incorrect password or name");
    }

    private void signUp() throws FileNotFoundException, UnsupportedEncodingException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write your name: ");
        String name = sc.next();
        System.out.println("Write your surname: ");
        String surname = sc.next();
        System.out.println("Write your username: ");
        String username = sc.next();
        System.out.println("Write your password: ");
        String password = sc.next();
        if (name != null && surname != null && username != null && password != null) {
            User user = new User();
            user.setName(name);
            user.setSurname(surname);
            user.setUsername(username);
            user.setPassword(password);
            if (user.getPassword() != null) {
                addUser(user);
                signedUser = user;
            }
        }
    }


    public void start() throws IOException {
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }

        // save the userlist to db.txt
        saveUserList();
    }

    private void saveUserList() throws IOException {
        String content = "";
        for (User user : userList)
        {
            content += user.getId() + " " + user.getName() + " " + user.getSurname() + " " +
                    user.getUsername() + " " + user.getPassword() + "\n";
        }

        Files.write(Paths.get("C:\\Users\\ACER\\Desktop\\db.txt"), content.getBytes());

    }
}
