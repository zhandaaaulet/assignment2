package com.company;

public class Animal extends User {
    private String pet;

    public Animal(int id, String name, String surname, String username, String password, String pet) {
        super(id, name, surname, username, password);
    }

    public String getPet() {
        return pet;
    }

public void setPet(String pet) {
        this.pet=pet;
}

    @Override
    public String toString() {
        return super.toString();
    }
}
